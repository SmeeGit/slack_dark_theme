#!/bin/sh

# shutdown slack first
SERVICE='Slack'
if ps ax | grep -v grep | grep $SERVICE > /dev/null
then
    echo "$SERVICE service running, shutting down"
    osascript -e 'quit app "Slack"'
fi

# sudo is needed since this script modifies an application file
# the file included here is the same as the app included file but then is augmented (overidden settings for the theme) at the bottom

# make a backup of the ssb-interop.js file [ssb-interop.js.bak] to revert if desired
sudo cp /Applications/Slack.app/Contents/Resources/app.asar.unpacked/src/static/ssb-interop.js /Applications/Slack.app/Contents/Resources/app.asar.unpacked/src/static/ssb-interop.js.bak
# copy the modified ssb-interop.js into the slack.app path to override the theme
sudo cp ./ssb-interop.js /Applications/Slack.app/Contents/Resources/app.asar.unpacked/src/static/
# open or re-open slack to see the new theme
open -a Slack


######### pull from remote if desired ##########
# alternate method if this file is changed a bunch and this source is updating with any changes

# curl http://neckcode.com/slack/ssb-interop.js.zip > ./temp.zip
# sudo cp /Applications/Slack.app/Contents/Resources/app.asar.unpacked/src/static/ssb-interop.js /Applications/Slack.app/Contents/Resources/app.asar.unpacked/src/static/ssb-interop.js.bak
# unzip -d /Applications/Slack.app/Contents/Resources/app.asar.unpacked/src/static ./temp.zip
